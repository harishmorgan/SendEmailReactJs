import React, { Component } from 'react'

export default class ClassComponents extends Component {
 state = {
    name: "Hello Harish"
  }

  render() {
    return (
      <div>
        <h2>Welcome to POD7 {this.state.name}</h2>
      </div>
    )
  }
}
