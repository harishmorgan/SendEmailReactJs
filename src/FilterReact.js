import React from 'react'

function FilterReact() {
    const names = ["Harish","Rudrani","Ravikant","Vidya"] 
    const filtered = names.filter(name => name.includes("R"))
  return (
    <div>
        {
      filtered.map(item => <li> {item}</li>
      ) }
    </div>
  )
}

export default FilterReact
