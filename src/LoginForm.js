import React,{useState} from 'react'

const LoginForm = () => {
    const [data,setData] = useState({
        username:'',
        password:''
    })
    const{username,password} = data;
    const ChangeHandler = e => [
        setData({...data,[e.target.name]:[e.target.value]})
    ]
    const submitHandler = e =>{
        e.preventDefault()
        console.log(data)
    }
  return (
    <div>
      <center>
        <form onSubmit={submitHandler}>
            <input type = "text" name ="username" value={username} onChange={ChangeHandler}  placeholder="username" /> <br />
            <input type ="password" name = "password" value={password} onChange={ChangeHandler}  placeholder="password" /> <br/>
            <input type = "submit" name = "submit" />
        </form>
      </center>
    </div>
  )
}

export default LoginForm
