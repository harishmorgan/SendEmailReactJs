import React,{useState} from 'react'
import axios from 'axios';
const FireDatabase = () => {
    const [data,setData] = useState({
        username:'',
        email:'',
        password:'',
        confirmPassword:''
    })
    const{username,email,password,confirmPassword } = data;
    const ChangeHandler = e => [
        setData({...data,[e.target.name]:e.target.value})
    ]
    const submitHandler = e =>{
        e.preventDefault();
        axios.post('https://hsbcevent-a43fa-default-rtdb.firebaseio.com/register.json',data).then(
            () => alert("submitted sucessfully")
        )

        if(username.length<=5){
            alert("username must be atleast 5 characters")
        }else if(password!==confirmPassword)
        {
            alert("Passwords are not matched")
        }else{
            console.log(data)
        }
    }
  return (
    <div>
      <center>
        <form onSubmit={submitHandler}>
            <input type = "text" name ="username" value={username} onChange={ChangeHandler}  placeholder="username" /> <br />
            <input type = "email" name = "email" value={email} onChange={ChangeHandler} placeholder="email" /> <br/>
            <input type ="password" name = "password" value={password} onChange={ChangeHandler}  placeholder="password" /> <br/>
            <input type = "password" name = "confirmPassword" value={confirmPassword} onChange={ChangeHandler} placeholder="confirmpassword" /> <br/>
            {password!==confirmPassword ? <p style={{"color":"red"}}>Passwords are not matching</p>:null}
            <input type = "submit" name = "submit" /> <br/>
        </form>
      </center>
    </div>
  )
}

export default FireDatabase
