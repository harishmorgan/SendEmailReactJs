// eslint-disable-next-line no-unused-vars
import React,{useEffect, useState} from 'react'
import axios from 'axios';
const FetchRequest = () => {
    const[data,setData] = useState([]);
    useEffect=(()=>{
        fetch('https://jsonplaceholder.typicode.com/todos').then(
            response => response.json()
        ).then(json => setData(json))
    },[])
  return (
    <div>
      {data.map(item => <li key={item.id}>{item.title}</li>)}
    </div>
  )
}

export default FetchRequest
