import React from 'react'

function MapFunctionsWithIndex() {
    const arr =["Harish","Rudrani","Ravikant","Vidya"] 
  return (
    <div>
      {
        arr.map(
            (value,index) => <li key ={index}>{value}
        </li>
  )
      }
    </div>
  )
}

export default MapFunctionsWithIndex
