import React, { useState, useEffect } from "react";
import axios from "axios";
const EmailTrigger = ({ customerId }) => {
  const [emailSent, setEmailSent] = useState(false);
  useEffect(() => {
    // Send an email to the customer
    axios.post(`/api/v1/customers/${customerId}/email`)
      .then((res) => {
        setEmailSent(true);
      })
      .catch((err) => {
        console.error(err);
      });
  }, [customerId]);
  if (emailSent) {
    return (
      <div>
        <p>An email has been sent to the customer.</p>
      </div>
    );
  }
  return (
    <div>
      <button onClick={() => setEmailSent(true)}>Send Email</button>
    </div>
  );
};
export default EmailTrigger;