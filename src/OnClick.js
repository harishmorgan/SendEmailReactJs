import React from 'react'

const OnClick = () => {
  return (
    <div>
      <button onClick={()=>console.log("clicked")} >Click Here</button>
    </div>
  )
}

export default OnClick