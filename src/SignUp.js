import React,{useState} from 'react'

const SignUp = () => {
    const [data,setData] = useState({
        username:'',
        email:'',
        password:'',
        confirmPassword:''
    })
    const{username,email,password,confirmPassword } = data;
    const ChangeHandler = e => [
        setData({...data,[e.target.name]:e.target.value})
    ]
    const submitHandler = e =>{
        e.preventDefault();

        if(password===confirmPassword){
            console.log(data)
        }else{
            console.log("Passwords are not matched")
        }
    }
  return (
    <div>
      <center>
        <form onSubmit={submitHandler}>
            <input type = "text" name ="username" value={username} onChange={ChangeHandler}  placeholder="username" /> <br />
            <input type = "email" name = "email" value={email} onChange={ChangeHandler} placeholder="email" /> <br/>
            <input type ="password" name = "password" value={password} onChange={ChangeHandler}  placeholder="password" /> <br/>
            <input type = "password" name = "confirmPassword" value={confirmPassword} onChange={ChangeHandler} placeholder="confirmpassword" /> <br/>
            <input type = "submit" name = "submit" /> <br/>
        </form>
      </center>
    </div>
  )
}

export default SignUp
