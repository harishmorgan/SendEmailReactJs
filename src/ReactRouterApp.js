import React from 'react'
import Navbar from './Navbar'
import { BrowserRouter,Route,Routes  } from 'react-router-dom';
import Home from './Home';
import AboutUs from './AboutUs';
import Dashboard from './Dashboard';
const ReactRouterApp = () => {
  return (
    <div>
        <BrowserRouter>
        <Navbar/>
        <Routes >
            <Route path="/" exact Component={Home}/>
            <Route path="/about" exact Component={AboutUs}/>
            <Route path="/dashboard/" exact Component={Dashboard}/>
        </Routes >
        </BrowserRouter>
    </div>
  )
}

export default ReactRouterApp
