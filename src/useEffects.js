import React,{useState,useEffect}from 'react'

const UseEffects = () => {
    const[count,setCount] = useState(0);
    useEffect(() => console.log(count),[count])
  return (
    <div>
      <p>
You clicked {count} times
    </p>
   <button onClick={()=>setCount (count+1)}>
    Clickme
    </button>
    </div>
  )
}

export default UseEffects