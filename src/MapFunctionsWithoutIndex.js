import React from 'react'

function MapFunctionsWithoutIndex() {
    const arr = [
        {
            id:1,
            title:"ReactJs"
        },
        {
            id:2,
            title:"Learners"
        },
        {
            id:3,
            title:"3 People"
        },
        {
            id:4,
            title:"POD7"
        }
    ]
  return (
    <div>
      {
        arr.map(
            (value) => <li key={value.id}>{value.title}</li>
        )
      }
    </div>
  )
}

export default MapFunctionsWithoutIndex
