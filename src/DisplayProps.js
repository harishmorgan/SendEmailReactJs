import React, { Component } from 'react'

export default class DisplayProps extends Component {
  render() {
    return (
      <div>
        <h4>{this.props.name}</h4>
      </div>
    )
  }
}
