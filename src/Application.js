import React from 'react'
import Header from './components/Header'
import Home from './components/Home'
import Footer from './components/Footer'
import { WithoutexportDefault } from './components/WithoutexportDefault'

const Application = () => {
  return (
    <div>
     <Header/>
     <Home/>
     <WithoutexportDefault/>
     <Footer/> 
    </div>
  )
}

export default Application
